﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static winAppGame.model.PaiRuleType;

namespace winAppGame.model
{
    /// <summary>
    /// author:zuowenjun
    /// copyright:www.zuowenjun.cn
    /// 扑克牌出牌规则检查结果
    /// </summary>
    public class PaiRuleCheckResult
    {
        public bool AllowShow { get; set; }

        public string ErrMsg { get; set; }

        public PaiRuleTypeItem RuleType { get; set; }

        public IDictionary<Pai,int> ShowPais { get; set; }

        public PaiRuleCheckResult() { }

        public PaiRuleCheckResult(bool allowShow, string errMsg)
        {
            AllowShow = allowShow;
            ErrMsg = errMsg;
        }

        public static PaiRuleCheckResult Pass(PaiRuleTypeItem ruleType = null, IDictionary<Pai, int> showPais =null)
        {
            return new PaiRuleCheckResult(true, string.Empty) { RuleType = ruleType,ShowPais=showPais };
        }

        public static PaiRuleCheckResult Failed(string errMsg)
        {
            return new PaiRuleCheckResult(false, errMsg);
        }
    }

}
