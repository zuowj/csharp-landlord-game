﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static winAppGame.model.PaiRuleType;

namespace winAppGame.model
{
    /// <summary>
    /// author:zuowenjun
    /// copyright:www.zuowenjun.cn
    /// 扑克牌出牌规则检查上下文（检查阶段各方法传递信息）
    /// </summary>
    public class PaiRuleCheckContext
    {
        public PaiRuleTypeItem SelectedRuleType { get; set; }

        public PaiRuleType.PaiRuleTypeItem LastShowRuleType { get; set; }
    }
}
