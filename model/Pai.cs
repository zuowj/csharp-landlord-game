﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winAppGame.model
{
    /// <summary>
    /// author:zuowenjun
    /// copyright:www.zuowenjun.cn
    /// 扑克牌信息
    /// </summary>
    public class Pai : IComparable<Pai>,ICloneable
    {
        public static readonly Pai minPai = new Pai("3", 3, PaiKind.HongTao);
        public static readonly Pai maxPai = new Pai("2", 15, PaiKind.HongTao);

        public static readonly Pai guiPai = new Pai("joker", 16, PaiKind.HeiTao);
        public static readonly Pai guiPlusPai = new Pai("Joker", 17, PaiKind.HongTao);

        public static readonly IDictionary<int, string> moreThan10PaiDic = new Dictionary<int, string>()
        {
            {11,"J"}, {12,"Q"}, {13,"K"}, {14,"A" }, {15,"2"}, {16,"joker"}, {17,"Joker"}
        };

        public string Name { get; set; }
        public int Value { get; set; }

        public PaiKind Kind { get; set; }

        public Pai(int value, PaiKind kind)
        {
            this.Value = value;
            this.Kind = kind;
            if (value <= 10)
            {
                this.Name = value.ToString();
            }
            else
            {
                this.Name = moreThan10PaiDic[value];
            }
        }

        public Pai(string name, int value, PaiKind kind)
        {
            Name = name;
            Value = value;
            Kind = kind;
        }

        public int CompareTo(Pai other)
        {
            if (other == null)
            {
                return -1;
            }

            return this.Value.CompareTo(other.Value);
        }

        public static implicit operator int(Pai pai)
        {
            if (pai == null)
            {
                throw new ArgumentNullException();
            }
            return pai.Value;
        }

        public static bool operator ==(Pai paiX, Pai paiY)
        {

            int xValue = 0;
            try
            {
                xValue = paiX.Value;
            }
            catch (NullReferenceException)
            {
            }

            int yValue = 0;
            try
            {
                yValue= paiY.Value;
            }
            catch (NullReferenceException)
            {
            }

            return xValue == yValue;
        }

        public static bool operator !=(Pai paiX, Pai paiY)
        {
            return !(paiX == paiY);
        }

        public override string ToString()
        {
            return Enum.GetName(typeof(PaiKind), Kind) + Name;
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if(!(obj is Pai) || obj==null)
            {
                return false;
            }

            return this.Value == (obj as Pai).Value;
        }

        public object Clone()
        {
            return new Pai(this.Name,this.Value, this.Kind);
        }
    }

    public enum PaiKind
    {
        [Description("♥")]
        HongTao = 1,

        [Description("♠")]
        HeiTao = 2,

        [Description("♦")]
        MeiHua = 3,

        [Description("♣")]
        FangKuai = 4
    }
}
