﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winAppGame.model
{
    /// <summary>
    /// author:zuowenjun
    /// copyright:www.zuowenjun.cn
    /// 扑克牌规则类型（即：牌型打法）
    /// </summary>
    public class PaiRuleType
    {

        private static readonly Dictionary<string, PaiRuleTypeItem> _Items = new Dictionary<string, PaiRuleTypeItem>();
        private static PaiRuleTypeItem NewItem(string code, string name, int priority)
        {
            if (!_Items.ContainsKey(code))
            {
                _Items[code] = new PaiRuleTypeItem(code, name, priority);
            }

            return _Items[code];
        }

        public static ReadOnlyDictionary<string, PaiRuleTypeItem> Items()
        {
            if (!_Items.HasItem())
            {
                var props = typeof(PaiRuleType).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
                foreach(var prop in props)
                {
                    prop.GetValue(null);
                }
            }

            return new ReadOnlyDictionary<string, PaiRuleTypeItem>(_Items.OrderBy(i => i.Value.Priority).ToDictionary(i => i.Key, i => i.Value));
        }

        public static PaiRuleTypeItem Single => NewItem("1", "单张", 9);
        public static PaiRuleTypeItem Double => NewItem("2", "对子", 8);

        public static PaiRuleTypeItem DoubleMore => NewItem("2+", "连对", 7);
        public static PaiRuleTypeItem Three => NewItem("3", "3张", 6);

        public static PaiRuleTypeItem ThreeMore => NewItem("3+", "3连顺子", 5);

        public static PaiRuleTypeItem ThreeWithOne => NewItem("3+1", "3带1", 4);

        public static PaiRuleTypeItem ThreeWithOneMore => NewItem("3+1+", "飞机", 2);

        public static PaiRuleTypeItem ThreeWithDouble => NewItem("3+2", "3带2", 3);

        public static PaiRuleTypeItem ThreeWithDoubleMore => NewItem("3+2+", "飞机", 2);
        public static PaiRuleTypeItem Four => NewItem("4", "炸弹", 12);
        public static PaiRuleTypeItem FourWithOne => NewItem("4+1", "4带1", 11);

        public static PaiRuleTypeItem FourWithOneMore => NewItem("4+1+", "顺子", 13);

        public static PaiRuleTypeItem FourWithDouble => NewItem("4+2", "4带2", 10);

        public static PaiRuleTypeItem FourWithDoubleMore => NewItem("4+2+", "顺子", 13);

        public static PaiRuleTypeItem FiveMore => NewItem("5+", "顺子", 1);

        public static PaiRuleTypeItem Max => NewItem("joker+", "王炸", 14);



        public class PaiRuleTypeItem
        {
            public string Code { get; private set; }

            public string Name { get; private set; }

            public int Priority { get; private set; }
            public PaiRuleTypeItem(string code, string name, int priority)
            {
                this.Code = code;
                this.Name = name;
                this.Priority = priority;
            }
        }
    }
}
