﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winAppGame
{
    public static class TypeExtends
    {
        public static bool HasItem<T>(this IEnumerable<T> items)
        {
            return items != null && items.Any();
        }
    }
}
