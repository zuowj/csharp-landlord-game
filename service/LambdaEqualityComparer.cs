﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winAppGame.service
{
    public class LambdaEqualityComparer<T> : IEqualityComparer<T>
    {
        private readonly Func<T, T, bool> compareFunc;
        public LambdaEqualityComparer(Func<T, T, bool> compareFunc)
        {
            this.compareFunc = compareFunc;
        }

        public bool Equals(T x, T y)
        {
            return compareFunc.Invoke(x, y);
        }

        public int GetHashCode(T obj)
        {
            return obj == null ? -1 : obj.GetHashCode();
        }
    }
}
