﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winAppGame.model;
using winAppGame.service;

namespace winAppGame.service.rulechecker
{
    /// <summary>
    /// author:zuowenjun
    /// copyright:www.zuowenjun.cn
    /// 对王炸出牌规则检查器
    /// </summary>
    public class JokerPaiRuleChecker : PaiRuleChecker
    {
        public override int ForBaseCount => 2;

        public override int ForIncrementCount => 0;

        public override List<PaiRuleType.PaiRuleTypeItem> ForRuleTypes => BuildPaiRuleTypeItems(PaiRuleType.Max);

        protected override PaiRuleCheckResult DoCheckAllowShowPais(List<Pai> selectedPais, List<Pai> lastShowPais, PaiRuleCheckContext checkContext)
        {
            return PaiRuleCheckResult.Pass(PaiRuleType.Max);
        }

        protected override PaiRuleCheckResult DoCheckSelectedPais(List<Pai> selectedPais, PaiRuleCheckContext checkContext)
        {
            if (selectedPais.Where(p => p == Pai.guiPai || p == Pai.guiPlusPai).Count() != 2)
            {
                return PaiRuleCheckResult.Failed("出牌规则不正确，必需是大王、小王组成一对！");
            }

            return PaiRuleCheckResult.Pass(PaiRuleType.Max);
        }

        public override PaiRuleCheckResult AutoSelectPais(IDictionary<Pai, int> groupPais, IEnumerable<Pai> lastShowPais, PaiRuleType.PaiRuleTypeItem showRuleType)
        {
            Dictionary<Pai, int> okPais = new Dictionary<Pai, int>();
            if (groupPais.ContainsKey(Pai.guiPai) && groupPais.ContainsKey(Pai.guiPlusPai))
            {
                okPais[Pai.guiPai] = 1;
                okPais[Pai.guiPlusPai] = 1;
                return PaiRuleCheckResult.Pass(PaiRuleType.Max, okPais);
            }
            return PaiRuleCheckResult.Failed("要不起！");
        }
    }
}
