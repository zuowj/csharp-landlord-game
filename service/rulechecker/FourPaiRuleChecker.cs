﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winAppGame.model;
using winAppGame.service;

namespace winAppGame.service.rulechecker
{
    /// <summary>
    /// author:zuowenjun
    /// copyright:www.zuowenjun.cn
    /// 4个相同牌（即：炸弹）出牌规则检查器
    /// </summary>
    public class FourPaiRuleChecker : PaiRuleChecker
    {
        public override int ForBaseCount => 4;

        public override int ForIncrementCount => 0;

        public override List<PaiRuleType.PaiRuleTypeItem> ForRuleTypes => BuildPaiRuleTypeItems(PaiRuleType.Four);

        protected override PaiRuleCheckResult DoCheckAllowShowPais(List<Pai> selectedPais, List<Pai> lastShowPais, PaiRuleCheckContext checkContext)
        {
            if (checkContext.LastShowRuleType != PaiRuleType.Four && checkContext.LastShowRuleType != PaiRuleType.Max)
            {
                //如果不是炸弹，则炸弹永远是大的
                return PaiRuleCheckResult.Pass(PaiRuleType.Four);
            }

            var compare = selectedPais[0].CompareTo(lastShowPais[0]);

            if (compare > 0 && checkContext.LastShowRuleType == PaiRuleType.Four)
            {
                return PaiRuleCheckResult.Pass(PaiRuleType.Four);
            }

            return PaiRuleCheckResult.Failed("出牌数值<=上家已出牌数值！");
        }

        protected override PaiRuleCheckResult DoCheckSelectedPais(List<Pai> selectedPais, PaiRuleCheckContext checkContext)
        {
            var paiSet = ConvertToSet(selectedPais);
            if (paiSet.Count != 1)
            {
                return PaiRuleCheckResult.Failed("出牌规则不正确，当为炸弹时必需4张牌数值相同！");
            }

            return PaiRuleCheckResult.Pass(PaiRuleType.Four);
        }

        public override PaiRuleCheckResult AutoSelectPais(IDictionary<Pai, int> groupPais, IEnumerable<Pai> lastShowPais, PaiRuleType.PaiRuleTypeItem showRuleType)
        {
            Dictionary<Pai, int> okPais = new Dictionary<Pai, int>();
            if (!groupPais.Any(kv => kv.Value == 4))
            {
                return PaiRuleCheckResult.Failed("要不起！");
            }

            if (lastShowPais.HasItem())
            {
                if (lastShowPais.Count() != 4 || ConvertToDictionary(lastShowPais).Count() != 1)
                {
                    var selectedGroupPai = groupPais.Where(kv => kv.Value == 4).OrderBy(kv => kv.Key).First();
                    okPais[selectedGroupPai.Key] = selectedGroupPai.Value;
                    return PaiRuleCheckResult.Pass(PaiRuleType.Four, okPais);
                }
                else
                {
                    var lastShowMinPai = lastShowPais.Min(p => p);
                    var selectedGroupPai = groupPais.Where(kv => kv.Value == 4 && kv.Key > lastShowMinPai).OrderBy(kv => kv.Key).FirstOrDefault();
                    if (selectedGroupPai.Value <= 0)
                    {
                        return PaiRuleCheckResult.Failed("要不起！");
                    }

                    okPais[selectedGroupPai.Key] = selectedGroupPai.Value;
                    return PaiRuleCheckResult.Pass(PaiRuleType.Four, okPais);
                }
            }
            else
            {
                var selectedGroupPai = groupPais.Where(kv => kv.Value == 4).OrderBy(kv => kv.Key).First();
                okPais[selectedGroupPai.Key] = selectedGroupPai.Value;
                return PaiRuleCheckResult.Pass(PaiRuleType.Four, okPais);
            }
        }
    }
}
