﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winAppGame.model;
using winAppGame.service;

namespace winAppGame.service.rulechecker
{
    /// <summary>
    /// author:zuowenjun
    /// copyright:www.zuowenjun.cn
    /// 3张、3张顺子出牌规则检查器
    /// </summary>
    public class ThreePaiRuleChecker : PaiRuleChecker
    {
        public override int ForBaseCount => 3;

        public override int ForIncrementCount => 3;

        public override List<PaiRuleType.PaiRuleTypeItem> ForRuleTypes => BuildPaiRuleTypeItems(PaiRuleType.Three, PaiRuleType.ThreeMore);

        protected override PaiRuleCheckResult DoCheckAllowShowPais(List<Pai> selectedPais, List<Pai> lastShowPais, PaiRuleCheckContext checkContext)
        {
            var compare = selectedPais.Min().CompareTo(lastShowPais.Min());
            if (compare > 0)
            {
                return PaiRuleCheckResult.Pass(checkContext.SelectedRuleType);
            }

            return PaiRuleCheckResult.Failed("出牌数值<=上家已出牌数值！");
        }

        protected override PaiRuleCheckResult DoCheckSelectedPais(List<Pai> selectedPais, PaiRuleCheckContext checkContext)
        {
            var paiDic = ConvertToDictionary(selectedPais);
            int threePaiCount = paiDic.Where(p => p.Value == 3).Count();

            if (threePaiCount * 3 != selectedPais.Count)
            {
                //1个3张 或 2个3张以上均可以
                return PaiRuleCheckResult.Failed("出牌张数不正确，要么3张相同，要么6张及以上！");
            }

            var paiOrderList = paiDic.Where(p => p.Value == 3).Select(p => p.Key).OrderBy(p => p).ToList();
            if (!IsSerial(paiOrderList))
            {
                return PaiRuleCheckResult.Failed("非连续！");
            }

            checkContext.SelectedRuleType = threePaiCount > 1 ? PaiRuleType.ThreeMore : PaiRuleType.Three;
            return PaiRuleCheckResult.Pass(checkContext.SelectedRuleType);
        }


        public override PaiRuleCheckResult AutoSelectPais(IDictionary<Pai, int> groupPais, IEnumerable<Pai> lastShowPais, PaiRuleType.PaiRuleTypeItem showRuleType)
        {
            var threeGroupPais = groupPais.Where(kv => kv.Value >= 3).OrderBy(kv => kv.Key).Select(kv => kv);

            if (lastShowPais.HasItem())
            {
                var lastShowThreePais = ConvertToDictionary(lastShowPais).Where(kv => kv.Value == 3).AsEnumerable();
                var lastShowMinPai = lastShowThreePais.Min(kv => kv.Key);
                threeGroupPais = threeGroupPais.Where(kv => kv.Key > lastShowMinPai).OrderBy(kv => kv.Key).Select(kv => kv);
                return DoAutoSelectPais(threeGroupPais, lastShowThreePais, showRuleType);
            }
            else
            {
                return DoAutoSelectPais(threeGroupPais, null, showRuleType);
            }
        }



        private PaiRuleCheckResult DoAutoSelectPais(IEnumerable<KeyValuePair<Pai, int>> threeGroupPais, IEnumerable<KeyValuePair<Pai, int>> lastShowThreePais, PaiRuleType.PaiRuleTypeItem showRuleType)
        {
            if (!threeGroupPais.HasItem() || threeGroupPais.Count() < lastShowThreePais.Count())
            {
                return PaiRuleCheckResult.Failed("要不起！");
            }

            Pai prePai = null;
            int forIndex = 0;
            Dictionary<Pai, int> okPais = new Dictionary<Pai, int>();
            int lowShowPaisCount = lastShowThreePais.HasItem() ? lastShowThreePais.Count() : 2;

            if (showRuleType == PaiRuleType.Three)
            {
                var selectedPaiKv = threeGroupPais.OrderBy(kv => kv.Value).First();
                okPais[selectedPaiKv.Key] = 3;
                return PaiRuleCheckResult.Pass(PaiRuleType.Three, okPais);
            }

            foreach (var kv in threeGroupPais)
            {
                forIndex++;

                if (prePai != null)
                {
                    if (prePai + 1 != kv.Key)
                    {
                        if (forIndex >= threeGroupPais.Count() || forIndex > lowShowPaisCount)
                        {
                            //若最后一张牌 或 已达到连续2个3张及以上，则中止
                            break;
                        }
                        //否则重置，重新检测连续
                        okPais.Clear();
                        forIndex = 1;
                    }
                    else if (lastShowThreePais.HasItem() && okPais.Count() == lastShowThreePais.Count())
                    {
                        //若满足上家出牌张数则中止
                        break;
                    }
                }

                prePai = kv.Key;
                okPais.Add(kv.Key, 3);
            }

            if (okPais.Count() < lowShowPaisCount)
            {
                //如果3张的小于2个连续 ，则说明无法构成3+3+N的出牌规则
                return PaiRuleCheckResult.Failed("要不起！");
            }

            return PaiRuleCheckResult.Pass(PaiRuleType.ThreeMore, okPais);

        }

    }
}
