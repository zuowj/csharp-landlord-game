﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winAppGame.model;
using winAppGame.service;

namespace winAppGame.service.rulechecker
{
    /// <summary>
    /// author:zuowenjun
    /// copyright:www.zuowenjun.cn
    /// 单张出牌规则检查器
    /// </summary>
    public class SinglePaiRuleChecker : PaiRuleChecker
    {
        public override int ForBaseCount => 1;

        public override int ForIncrementCount => 0;

        public override List<PaiRuleType.PaiRuleTypeItem> ForRuleTypes => BuildPaiRuleTypeItems(PaiRuleType.Single);

        protected override PaiRuleCheckResult DoCheckAllowShowPais(List<Pai> selectedPais, List<Pai> lastShowPais, PaiRuleCheckContext checkContext)
        {
            var compare = selectedPais[0].CompareTo(lastShowPais[0]);
            if (compare > 0)
            {
                return PaiRuleCheckResult.Pass(PaiRuleType.Single);
            }

            return PaiRuleCheckResult.Failed("出牌数值<=上家已出牌数值！");
        }

        protected override PaiRuleCheckResult DoCheckSelectedPais(List<Pai> selectedPais, PaiRuleCheckContext checkContext)
        {
            return PaiRuleCheckResult.Pass(PaiRuleType.Single);
        }

        public override PaiRuleCheckResult AutoSelectPais(IDictionary<Pai, int> groupPais, IEnumerable<Pai> lastShowPais, PaiRuleType.PaiRuleTypeItem showRuleType)
        {
            int showMinPaiValue = lastShowPais.HasItem() ? lastShowPais.Min() : 0;
            var selectedPai = groupPais.Where(kv => kv.Value >= 1 && kv.Key > showMinPaiValue).OrderBy(kv => kv.Value).OrderBy(kv => kv.Key).Select(kv => kv.Key).FirstOrDefault();
            if (selectedPai == null)
            {
                return PaiRuleCheckResult.Failed("要不起！");
            }

            return PaiRuleCheckResult.Pass(PaiRuleType.Single, new Dictionary<Pai, int>() { { selectedPai, 1 } });
        }
    }
}
