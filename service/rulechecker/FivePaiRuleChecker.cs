﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winAppGame.model;
using winAppGame.service;

namespace winAppGame.service.rulechecker
{
    /// <summary>
    /// author:zuowenjun
    /// copyright:www.zuowenjun.cn
    /// 对5张牌及以上顺子出牌规则检查器
    /// </summary>
    public class FivePaiRuleChecker : PaiRuleChecker
    {
        public override int ForBaseCount => 5;

        public override int ForIncrementCount => 1;

        public override List<PaiRuleType.PaiRuleTypeItem> ForRuleTypes => BuildPaiRuleTypeItems(PaiRuleType.FiveMore);

        protected override PaiRuleCheckResult DoCheckAllowShowPais(List<Pai> selectedPais, List<Pai> lastShowPais, PaiRuleCheckContext checkContext)
        {
            var compare = selectedPais.Min().CompareTo(lastShowPais.Min());

            if (compare > 0)
            {
                return PaiRuleCheckResult.Pass(PaiRuleType.FiveMore);
            }

            return PaiRuleCheckResult.Failed("出牌数值<=上家已出牌数值！");
        }

        protected override PaiRuleCheckResult DoCheckSelectedPais(List<Pai> selectedPais, PaiRuleCheckContext checkContext)
        {
            var paiSet = ConvertToSet(selectedPais);
            if (paiSet.Count != selectedPais.Count || !IsSerial(selectedPais))
            {
                return PaiRuleCheckResult.Failed("出牌张数不正确，顺子至少5张牌且必需连续不能重复！");
            }

            return PaiRuleCheckResult.Pass(PaiRuleType.FiveMore);
        }


        public override PaiRuleCheckResult AutoSelectPais(IDictionary<Pai, int> groupPais, IEnumerable<Pai> lastShowPais, PaiRuleType.PaiRuleTypeItem showRuleType)
        {
            if (groupPais.Count() < ForBaseCount)
            {
                return PaiRuleCheckResult.Failed("要不起！");
            }

            int minPaiValue = 0;
            int lowShowPaisCount = ForBaseCount;

            if (lastShowPais.HasItem())
            {
                if (groupPais.Count() < lastShowPais.Count())
                {
                    return PaiRuleCheckResult.Failed("要不起！");
                }
                minPaiValue = lastShowPais.Min();
                lowShowPaisCount = lastShowPais.Count();
            }

            var selectedPais = groupPais.OrderBy(p => p.Key).Where(p => p.Key.Value > minPaiValue);
            if (!selectedPais.HasItem() || selectedPais.Count() < lowShowPaisCount)
            {
                return PaiRuleCheckResult.Failed("要不起！");
            }

            Pai prePai = null;
            int forIndex = 0;
            Dictionary<Pai, int> okPais = new Dictionary<Pai, int>();

            foreach (var kv in selectedPais)
            {
                forIndex++;

                if (prePai != null)
                {
                    if (prePai + 1 != kv.Key)
                    {
                        if (forIndex >= groupPais.Count() || forIndex > lowShowPaisCount)
                        {
                            //若最后一张牌 或 已达到连续5张及以上，则中止
                            break;
                        }
                        //否则重置，重新检测连续
                        okPais.Clear();
                        forIndex = 1;
                    }
                    else if (lastShowPais.HasItem() && okPais.Count() == lastShowPais.Count())
                    {
                        //若满足上家出牌张数则中止
                        break;
                    }
                }

                prePai = kv.Key;
                okPais.Add(kv.Key, 1);
            }

            if (okPais.Count < lowShowPaisCount)
            {
                return PaiRuleCheckResult.Failed("要不起！");
            }

            return PaiRuleCheckResult.Pass(PaiRuleType.FiveMore, okPais);
        }

    }
}
