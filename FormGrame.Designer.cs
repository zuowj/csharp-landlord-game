﻿namespace winAppGame
{
    partial class FormGrame
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.labelPlayer1 = new System.Windows.Forms.Label();
            this.labelPlayer2 = new System.Windows.Forms.Label();
            this.labelPlayer3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panelShow = new System.Windows.Forms.Panel();
            this.buttonPass1 = new System.Windows.Forms.Button();
            this.buttonPass2 = new System.Windows.Forms.Button();
            this.buttonPass3 = new System.Windows.Forms.Button();
            this.labelShowTip = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.panel1.Location = new System.Drawing.Point(82, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(726, 100);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel2.Location = new System.Drawing.Point(82, 144);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 335);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel3.Location = new System.Drawing.Point(608, 144);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 335);
            this.panel3.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(833, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "发牌";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "人脑方：";
            // 
            // comboBox1
            // 
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "横1方",
            "竖2方",
            "竖3方"});
            this.comboBox1.Location = new System.Drawing.Point(6, 47);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(92, 20);
            this.comboBox1.TabIndex = 5;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // labelPlayer1
            // 
            this.labelPlayer1.AutoSize = true;
            this.labelPlayer1.Location = new System.Drawing.Point(35, 12);
            this.labelPlayer1.Name = "labelPlayer1";
            this.labelPlayer1.Size = new System.Drawing.Size(35, 12);
            this.labelPlayer1.TabIndex = 6;
            this.labelPlayer1.Text = "横1方";
            // 
            // labelPlayer2
            // 
            this.labelPlayer2.AutoSize = true;
            this.labelPlayer2.Location = new System.Drawing.Point(35, 144);
            this.labelPlayer2.Name = "labelPlayer2";
            this.labelPlayer2.Size = new System.Drawing.Size(35, 12);
            this.labelPlayer2.TabIndex = 7;
            this.labelPlayer2.Text = "竖2方";
            this.labelPlayer2.Click += new System.EventHandler(this.labelPlayer2_Click);
            // 
            // labelPlayer3
            // 
            this.labelPlayer3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPlayer3.AutoSize = true;
            this.labelPlayer3.Location = new System.Drawing.Point(567, 144);
            this.labelPlayer3.Name = "labelPlayer3";
            this.labelPlayer3.Size = new System.Drawing.Size(35, 12);
            this.labelPlayer3.TabIndex = 8;
            this.labelPlayer3.Text = "竖3方";
            this.labelPlayer3.Click += new System.EventHandler(this.label2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Location = new System.Drawing.Point(823, 56);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(114, 122);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "设置";
            // 
            // panelShow
            // 
            this.panelShow.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelShow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelShow.Location = new System.Drawing.Point(315, 144);
            this.panelShow.Name = "panelShow";
            this.panelShow.Size = new System.Drawing.Size(209, 335);
            this.panelShow.TabIndex = 10;
            // 
            // buttonPass1
            // 
            this.buttonPass1.Enabled = false;
            this.buttonPass1.Location = new System.Drawing.Point(12, 27);
            this.buttonPass1.Name = "buttonPass1";
            this.buttonPass1.Size = new System.Drawing.Size(64, 23);
            this.buttonPass1.TabIndex = 11;
            this.buttonPass1.Text = "要不起";
            this.buttonPass1.UseVisualStyleBackColor = true;
            this.buttonPass1.Click += new System.EventHandler(this.buttonPass_Click);
            // 
            // buttonPass2
            // 
            this.buttonPass2.Enabled = false;
            this.buttonPass2.Location = new System.Drawing.Point(12, 159);
            this.buttonPass2.Name = "buttonPass2";
            this.buttonPass2.Size = new System.Drawing.Size(64, 23);
            this.buttonPass2.TabIndex = 12;
            this.buttonPass2.Text = "要不起";
            this.buttonPass2.UseVisualStyleBackColor = true;
            this.buttonPass2.Click += new System.EventHandler(this.buttonPass_Click);
            // 
            // buttonPass3
            // 
            this.buttonPass3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPass3.Enabled = false;
            this.buttonPass3.Location = new System.Drawing.Point(538, 159);
            this.buttonPass3.Name = "buttonPass3";
            this.buttonPass3.Size = new System.Drawing.Size(64, 23);
            this.buttonPass3.TabIndex = 13;
            this.buttonPass3.Text = "要不起";
            this.buttonPass3.UseVisualStyleBackColor = true;
            this.buttonPass3.Click += new System.EventHandler(this.buttonPass_Click);
            // 
            // labelShowTip
            // 
            this.labelShowTip.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelShowTip.AutoSize = true;
            this.labelShowTip.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelShowTip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelShowTip.Location = new System.Drawing.Point(435, 119);
            this.labelShowTip.Name = "labelShowTip";
            this.labelShowTip.Size = new System.Drawing.Size(142, 19);
            this.labelShowTip.TabIndex = 14;
            this.labelShowTip.Text = "请开始出牌吧！";
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(833, 205);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 15;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Visible = false;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // FormGrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 491);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.labelShowTip);
            this.Controls.Add(this.buttonPass3);
            this.Controls.Add(this.buttonPass2);
            this.Controls.Add(this.buttonPass1);
            this.Controls.Add(this.panelShow);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labelPlayer3);
            this.Controls.Add(this.labelPlayer2);
            this.Controls.Add(this.labelPlayer1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "FormGrame";
            this.Text = "文俊斗地主";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.FormGrame_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label labelPlayer1;
        private System.Windows.Forms.Label labelPlayer2;
        private System.Windows.Forms.Label labelPlayer3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panelShow;
        private System.Windows.Forms.Button buttonPass1;
        private System.Windows.Forms.Button buttonPass2;
        private System.Windows.Forms.Button buttonPass3;
        private System.Windows.Forms.Label labelShowTip;
        private System.Windows.Forms.Button btnTest;
    }
}

